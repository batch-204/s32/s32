let http = require("http");

let directory = [
	{
		"name": "Brandon",
		"email": "brandon@mail.com"
	},
	{
		"name": "Jobert",
		"email": "jobert@mail.com"
	}
]

http.createServer(function (request, response) {

	if (request.url == "/users" && request.method == "GET") {

		response.writeHead(200, {'Content-Type': 'application/json'});
		response.write(JSON.stringify(directory));
		response.end()
	}

	if (request.url == '/users' && request.method == 'POST') {

		// 1. Declare a placeholder variable in order to be re-assigned later on. This variable will be assigned the data inside the body from Postman. 
		let request_body = ''

		// 2. When data is detected, run a function that assigns that data to the empty request_body variable
		// Request, sa taas yun. Yung on yung binder. 
		// Kapag on, nakikinig siya sa pumapasok na data. 
		// Yung unang data is type of event. Tapos yung data sa loob ng function is yung data mismo. 
		// Yang += nagassign lang yan, pwede din namang dalawang equal yan.
		request.on('data', function(data){
			console.log(data)
			request_body += data
		})

		// 3. Before the request ends, we want to add the new data to our existing users array of objects. 
		request.on('end', function() {
			console.log(typeof request_body)
			console.log(request_body)

			// 4. Convert the request_body from JSON to a JS Object, then assign that converted value back to the request_body variable.
			// Parse, yung JSON body is JSON. Tinatranslate nila into regular string
			request_body = JSON.parse(request_body)
			console.log(request_body)
			console.log(request_body.name)
			console.log(request_body.email)

			// 5. Declare a new_user variable signifying the new data that came from the Postman body
			// Kaya pinaparse yung request body para maaccess si name and email  na nasa postman
			// Object to. 
			let new_directory = {
				"name" : request_body.name,
				"email": request_body.email
			}

			// 6. Add the new_user variable (object) into the users array.
			directory.push(new_directory)
			console.log(directory)

			// 7. Write the headers for the response. Make sure the content is 'application/json' since we are writing/returning a JSON
			response.writeHead(200, {'Content-Type': 'application/json'})
			// Header
			response.write(JSON.stringify(new_directory))
			// Stringify - Ginagawang JSON
			response.end()
		})
	}
}).listen(4000);

console.log('Server running at localhost:4000');


// Pagdating ng data kay server, ang babasahin is text. Ang naiintindihan ni server is JSON lang. Kaya dapat tinatranslate na server. 
// 